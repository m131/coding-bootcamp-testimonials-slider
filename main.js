'use strict';

document.addEventListener('DOMContentLoaded', () => {
	const slides = document.querySelectorAll('.slide');
	const prevButton = document.querySelector('.slider-controls--prev');
	const nextButton = document.querySelector('.slider-controls--next');
	let currentSlide = document.querySelector('.slide--active');
	
	nextButton.addEventListener('click', e => {
		e.preventDefault();
		if(currentSlide.nextElementSibling){
			currentSlide.classList.remove('slide--active');
			currentSlide.nextElementSibling.classList.add('slide--active');
			currentSlide = currentSlide.nextElementSibling;
		}
		else{
			slides[slides.length - 1].classList.remove('slide--active');
			slides[0].classList.add('slide--active');
			currentSlide = slides[0];
		}
	});

	prevButton.addEventListener('click', e => {
		e.preventDefault();
		if(currentSlide.previousElementSibling){
			currentSlide.classList.remove('slide--active');
			currentSlide.previousElementSibling.classList.add('slide--active');
			currentSlide = currentSlide.previousElementSibling;
		}
		else{
			slides[0].classList.remove('slide--active');
			slides[slides.length - 1].classList.add('slide--active');
			currentSlide = slides[slides.length - 1];
		}
	});
	
});
